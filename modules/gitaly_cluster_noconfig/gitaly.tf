resource "aws_security_group" "gitaly_cluster" {
  name        = "gitlab-app-sg-gitaly"
  description = "gitaly security group"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 8075
    to_port     = 8075
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
    #security_groups = [var.intra-lb-security-group]
  }

  ingress {
    from_port   = 9999
    to_port     = 9999
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
    #security_groups = [var.intra-lb-security-group]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
    # security_groups = [var.vpn_security_group]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "praefect" {
  name        = "gitlab-app-sg-praefect"
  description = "praefect security group"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 2305
    to_port     = 2305
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }

  ingress {
    from_port   = 3305
    to_port     = 3305
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "gitaly_server" {
  count         = var.gitaly_count
  instance_type = var.gitaly_instance_type
  ami           = var.ami_id
  key_name      = var.key_name
  subnet_id     = element(var.private_subnets, count.index)
  ebs_optimized = true

  iam_instance_profile = aws_iam_instance_profile.gitaly.name
  vpc_security_group_ids = [aws_security_group.gitaly_cluster.id]
  tags = {
    Name = "gitaly-${count.index + 1}"
  }
  user_data = base64encode(templatefile("${path.module}/templates/user_data.yml",{}))
}

resource "aws_ebs_volume" "gitlab_gitaly" {
  count             = var.gitaly_count
  availability_zone = aws_instance.gitaly_server[count.index].availability_zone
  type              = "gp2"
  encrypted         = true
  size              = var.git_size
}

resource "aws_volume_attachment" "gitlab_attachment" {
  count       = var.gitaly_count
  device_name = "/dev/xvdf"
  volume_id   = aws_ebs_volume.gitlab_gitaly[count.index].id
  instance_id = aws_instance.gitaly_server[count.index].id
}

resource "aws_instance" "praefect_server" {
  count         = var.praefect_count
  instance_type = var.praefect_instance_type
  ami           = var.ami_id
  key_name      = var.key_name
  subnet_id     = element(var.private_subnets, count.index)
  ebs_optimized = true

  iam_instance_profile   = aws_iam_instance_profile.gitaly.name
  vpc_security_group_ids = [aws_security_group.praefect.id]
  tags = {
    Name = "praefect-${count.index + 1}"
  }
user_data = base64encode(templatefile("${path.module}/templates/user_data.yml",{}))
}


resource "aws_network_interface" "praefect" {
  count           = var.praefect_count
  subnet_id       = element(var.private_subnets, count.index)
  private_ips     = [var.praefect_ips[count.index]]
  security_groups = [aws_security_group.praefect.id]

  attachment {
    instance     = aws_instance.praefect_server[count.index].id
    device_index = 1
  }
}

