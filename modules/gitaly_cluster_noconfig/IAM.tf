resource "aws_iam_instance_profile" "gitaly" {
  name = "gitaly-nodes"
  path = aws_iam_role.gitaly.path
  role = aws_iam_role.gitaly.name
}

resource "aws_iam_role" "gitaly" {
  name               = "gitaly-nodes"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.gitaly-assume-role.json
}

data "aws_iam_policy_document" "gitaly-assume-role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      identifiers = ["ec2.amazonaws.com"]
      type        = "Service"
    }
  }
}

# This is just for GitLab Internal testing so a key pair is not needed
resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.gitaly.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}