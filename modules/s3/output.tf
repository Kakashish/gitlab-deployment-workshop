output "artifacts" {
  value = join("-", [var.prefix, "gitlab", "artifacts", "${random_string.random.result}"])
}

output "external_diffs" {
  value = join("-", [var.prefix, "gitlab", "externaldiffs", "${random_string.random.result}"])
}

output "lfs" {
  value = join("-", [var.prefix, "gitlab", "lfs", "${random_string.random.result}"])
}

output "uploads" {
  value = join("-", [var.prefix, "gitlab", "uploads", "${random_string.random.result}"])
}

output "packages" {
  value = join("-", [var.prefix, "gitlab", "packages", "${random_string.random.result}"])
}

output "dependency-proxy" {
  value = join("-", [var.prefix, "gitlab", "dependency-proxy", "${random_string.random.result}"])
}

output "tfstate" {
  value = join("-", [var.prefix, "gitlab", "tfstate", "${random_string.random.result}"])
}

output "backup" {
  value = join("-", [var.prefix, "gitlab", "backup", "${random_string.random.result}"])
}
