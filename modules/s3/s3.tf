# s3 bucket

resource "random_string" "random" {
  length  = 3
  special = false
  number  = false
  upper   = false
}

resource "aws_s3_bucket" "this" {
  for_each = toset(var.gitlab_s3_buckets)
  bucket   = join("-", [var.prefix, "gitlab", each.value, "${random_string.random.result}"])
  # true for config bucket
  acl = "private"

  versioning {
    enabled = false
  }

  dynamic "server_side_encryption_configuration" {
    for_each = var.s3_kms_key_arn != null ? [""] : []

    content {
      rule {
        apply_server_side_encryption_by_default {
          sse_algorithm     = "aws:kms"
          kms_master_key_id = var.s3_kms_key_arn
        }
      }
    }
  }
  dynamic "server_side_encryption_configuration" {
    for_each = var.s3_kms_key_arn == null ? [""] : []

    content {
      rule {
        apply_server_side_encryption_by_default {
          sse_algorithm = "AES256"
        }
      }
    }
  }

  tags = var.tags
}
