Role Name
=========

This role Configures Prometheus and Grafana Stand Alone

Requirements
------------

None

Role Variables
--------------

external_url                    : The URL Used for GitLab
grafana_admin_pwd               : Default admin pwd for Grafana
monitoring_whitelist            : CIDR Whitelist for Monitoring



Optional
consul_servers                  : Space Sperated Consule List EX: 123.123.123.3 123.123.123.4 123.123.123.5 
smtp_from                       : noreply@domain.com
smtp_host                       : exchange.squaresoft.com
smtp_port                       : 25

Dependencies
------------

None 

Author Information
------------------

kvogt@gitlab.com